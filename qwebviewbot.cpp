#include "qwebviewbot.h"
#include "qwebpagebot.h"
#include <QWebFrame>
#include <QWebElementCollection>
#include <QWebElement>
#include <QWheelEvent>
#include <QCoreApplication>
#include <QNetworkRequest>
#include <QMutex>
QMutex mutex;
QMutex mutex_scrollto;

QWebViewBot::QWebViewBot(QWidget *parent) :
    QWebView(parent)
{
    useragent   = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0";
    status_load = false;

    lastPoint = QPoint();

    QWebPageBot *page = new QWebPageBot();
    setPage(page);

    connect(this, SIGNAL(loadStarted()), this, SLOT(set_status_loading()));
    connect(this, SIGNAL(loadFinished(bool)), this, SLOT(set_status_loaded(bool)));
    setMouseTracking(true);

    shiftPressed = false;
    leftButtonPressed = false;
}

QWebView* QWebViewBot::createWindow(QWebPage::WebWindowType type)
{
    Q_UNUSED(type);

    this->parent->create_tab("","");
    return this->parent->actionTab();
}

void QWebViewBot::setUserAgent(const QString & string_useragent)
{
    useragent = string_useragent;
    getPage()->setAgent(string_useragent);
}

QWebPageBot* QWebViewBot::getPage()
{
    return (QWebPageBot*)page();
}

QUrl QWebViewBot::getParsedUrl()
{
    return this->url();
}

QWebElementCollection QWebViewBot::findAllBySelector(const QString & selector )
{
    return page()->mainFrame()->findAllElements(selector);
}

void QWebViewBot::setResolution(const int & width_int, const int & height_int)
{
    setFixedSize(width_int,height_int);
}

void QWebViewBot::scrollTo(const int & y, const Qt::Orientation & orientation)
{
    mutex_scrollto.lock();
    page()->mainFrame()->setScrollBarValue(orientation, y);
    mutex_scrollto.unlock();
}

void QWebViewBot::scrollOn( const int & num, const Qt::Orientation & orientation )
{
    mutex.lock();
    scrollTo( (orientation == Qt::Vertical ? page()->mainFrame()->scrollPosition().y() : page()->mainFrame()->scrollPosition().x()) + num, orientation);
    mutex.unlock();
}

QString QWebViewBot::get_element_attr( const QWebElement & el, const QString & name )
{
    return el.attribute(name);
}

QRect QWebViewBot::get_element_geometry( const QWebElement & el )
{
    QWebElement mel(el);
    qDebug() << el.geometry();
    qDebug() << mel.geometry();


    return el.geometry();
}

QString QWebViewBot::getCurrentUrl()
{
    return page()->mainFrame()->url().url();
}

// ------------- Slots
void QWebViewBot::load_url(const QString & url, const QString & referer )
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    if(!referer.isNull())
    {
       request.setRawHeader("Referer", referer.toUtf8());
    }
    load(request);
}

void QWebViewBot::set_status_loading()
{
    qDebug() << "Loading page!";
    status_load = true;
}

void QWebViewBot::click_on_coords( const int & x, const int & y)
{
    getPage()->clickOnCoords(x,y);
}

bool QWebViewBot::click_on_first_find( const QString & selector )
{
    return getPage()->clickOnFirstFind(selector);
}

void QWebViewBot::set_status_loaded(bool status)
{
    Q_UNUSED(status);
    qDebug() << "Loading is finished!";
    status_load = false;
    QWebElementCollection coll = this->findAllBySelector("[wmode='direct']");
    foreach( QWebElement el, coll)
    {
        qDebug() << "Removed - " << el.tagName();
        el.evaluateJavaScript("this.removeAttribute('wmode');");
    }
}

QPoint QWebViewBot::get_coords_by_selector( const QString & selector )
{
    QWebElement element = page()->mainFrame()->findFirstElement(selector);
    if(element.isNull()) return QPoint(0,0);

    return element.geometry().center();
}

void QWebViewBot::saveFrameImage(const QString & filename)
{
    QImage * image1 = new QImage(width(), height(), QImage::Format_RGB32);
    QPainter  * painter = new QPainter(image1);
    page()->mainFrame()->render(painter);
    painter->end();
    image1->save(filename+".png", "PNG", 100);
}

QPoint QWebViewBot::getImagePoint(const QString & base64_png, const int & accuracy)
{
    QImage * image1 = new QImage(width(), height(), QImage::Format_RGB32);
    QPainter  * painter = new QPainter(image1);
    page()->mainFrame()->render(painter);
    painter->end();

    QByteArray ba_s;
    ba_s.append(base64_png);

    QByteArray by = QByteArray::fromBase64(ba_s);
    QImage image = QImage::fromData(by,"PNG");

    int accuracy_count_pixels = ((image.height() * image.width()) / 100) * accuracy;

    QPoint point;
    int sum_m = 0;

    for( int source_x = 0; width() > source_x; source_x++ )
    {
        for( int source_y = 0; height() > source_y; source_y++ )
        {
            if(image1->pixel(source_x,source_y) == image.pixel(0,0))
            {
                int matches = 0;
                for( int search_x = 0; image.width() > search_x; search_x++)
                 {
                     for( int search_y = 0; image.height() > search_y; search_y++)
                     {
                        if(height() > source_y+search_y && width() > source_x+search_x )
                        {
                            if(image1->pixel(source_x+search_x, source_y+search_y) == image.pixel(search_x,search_y))
                                matches++;
                        }
                     }
                 }
                if(matches > accuracy_count_pixels && sum_m < matches)
                {
                    point = QPoint(source_x,source_y);
                    sum_m = matches;
                }
            }
        }
    }
    return point;
}

void QWebViewBot::send_wheel_scroll(const int & delta, const Qt::Orientation & orientation)
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;

    QWheelEvent *mp = new QWheelEvent(QPointF(100.0,100.0), delta, Qt::NoButton, mod, orientation );
    QCoreApplication::sendEvent(this, mp);
}

QWebElementCollection QWebViewBot::get_collection_by_selector( const QString & selector )
{
    return this->page()->mainFrame()->findAllElements(selector);
}

void QWebViewBot::send_mouse_move_event(const QPoint & point )
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;

    Qt::MouseButton buttonMod = Qt::NoButton;
    if(leftButtonPressed) buttonMod = Qt::LeftButton;

    QMouseEvent mp(QEvent::MouseMove, point, buttonMod, buttonMod, mod);
    QCoreApplication::sendEvent(this, &mp);

    //QPaintEvent * e = new QPaintEvent(QRect(lastPoint.x(),lastPoint.y(),7,7));
    //this->paintEvent(e);
    this->update();

    lastPoint = point;

    emit cursorMoved(point);
}

void QWebViewBot::paintEvent( QPaintEvent * e )
{
    QWebView::paintEvent(e);

    QPainter p(this);
    p.setPen(QPen(Qt::black, 5));
    p.drawPoint(lastPoint);
}

void QWebViewBot::wheelEvent( QWheelEvent * e )
{
    QWebView::wheelEvent(e);

    QPainter p(this);
    p.setPen(QPen(Qt::black, 5));
    p.drawPoint(lastPoint);
}

void QWebViewBot::resizeEvent( QResizeEvent * e )
{
    QWebView::resizeEvent(e);

    QPainter p(this);
    p.setPen(QPen(Qt::black, 5));
    p.drawPoint(lastPoint);
}

int QWebViewBot::get_element_scrollbar_value( QWebElement & e, const Qt::Orientation orien )
{
    Q_UNUSED(e)
    Q_UNUSED(orien)
    return 1;
}

void QWebViewBot::send_press_key(const QChar & key )
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QKeyEvent mp(QKeyEvent::KeyPress, key.unicode(), mod,key,false,1);
    QCoreApplication::sendEvent(this, &mp);
}

void QWebViewBot::send_release_key(const QChar & key )
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QKeyEvent mp(QKeyEvent::KeyRelease, key.unicode(), mod,key,false, 1);
    QCoreApplication::sendEvent(this, &mp);
}

void QWebViewBot::send_press_key(const int & key )
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QKeyEvent mp(QKeyEvent::KeyPress, key, mod);
    if(key == 0x01000020)
        shiftPressed = true;
    QCoreApplication::sendEvent(this, &mp);
}

void QWebViewBot::send_release_key(const int & key )
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QKeyEvent mp(QKeyEvent::KeyRelease, key, mod);
    if(key == 0x01000020)
        shiftPressed = false;
    QCoreApplication::sendEvent(this, &mp);
}


QWebElementCollection QWebViewBot::get_internal_url(const QString & substr, const QString & host)
{
    QWebElementCollection collection = findAllBySelector("a[href*='"+host+"'][href*='"+substr+"']:not([href='"+getCurrentUrl()+"'])");
    return collection;
}

QWebElementCollection QWebViewBot::get_external_url(const QString & substr)
{

    QWebElementCollection collection = findAllBySelector("a[href*='"+substr+"']");
    return collection;
}

void QWebViewBot::send_click_event_on_coords(const QPoint & point)
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QPoint p(point.x() - this->page()->mainFrame()->scrollPosition().x(), point.y() - this->page()->mainFrame()->scrollPosition().y());
    QMouseEvent mp(QMouseEvent::MouseButtonPress, p, Qt::LeftButton, Qt::LeftButton, mod);
    QCoreApplication::sendEvent(this, &mp);

    leftButtonPressed = true;
}

void QWebViewBot::send_release_event_on_coords(const QPoint & point)
{
    Qt::KeyboardModifier mod = Qt::NoModifier;
    if(shiftPressed) mod = Qt::ShiftModifier;
    QPoint p(point.x() - this->page()->mainFrame()->scrollPosition().x(), point.y() - this->page()->mainFrame()->scrollPosition().y());
    QMouseEvent mr(QMouseEvent::MouseButtonRelease, p, Qt::LeftButton, Qt::LeftButton, mod);
    QCoreApplication::sendEvent(this, &mr);

    leftButtonPressed = false;
}
