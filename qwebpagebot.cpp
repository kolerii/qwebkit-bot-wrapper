#include "qwebpagebot.h"
#include <QMouseEvent>
#include <QEvent>
#include <QApplication>
#include <QWebView>
#include <QWebFrame>
#include <QWebElement>
#include <QTimer>
#include "wa_math_space.cpp"

QWebPageBot::QWebPageBot(QObject *parent) :
    QWebPage(parent)
{
    _useragent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0";
}

QString QWebPageBot::userAgentForUrl ( const QUrl & url ) const
{
    Q_UNUSED(url)
    return _useragent;
}

void QWebPageBot::setAgent( const QString & useragent )
{
    _useragent = useragent;
}

void QWebPageBot::load(const QString & url)
{
    this->mainFrame()->load(QUrl(url));
}

QString QWebPageBot::chooseFile(QWebFrame *originatingFrame, const QString &oldFile)
{
    Q_UNUSED(originatingFrame)
    Q_UNUSED(oldFile)
    qDebug() << "choose";
    return "H:\\f_9745246c63fc0fd6.jpg";
}

void QWebPageBot::clickOnCoords(int x, int y)
{
    QMouseEvent mp(QEvent::MouseButtonPress, QPoint(x,y), Qt::LeftButton , Qt::LeftButton, Qt::NoModifier);
    QApplication::sendEvent(view(), &mp);

    QMouseEvent mr(QEvent::MouseButtonRelease, QPoint(x,y), Qt::LeftButton , Qt::LeftButton, Qt::NoModifier);
    QApplication::sendEvent(view(), &mr);
}

bool QWebPageBot::clickOnFirstFind(const QString & selector)
{
    QWebElement element = this->mainFrame()->findFirstElement(selector);

    if(element.isNull()) return false;
    qDebug() << "Element is found!";

    element.setFocus();
    QMouseEvent mp(QMouseEvent::MouseButtonPress, element.geometry().center(), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
    QCoreApplication::sendEvent(view(), &mp);

    QMouseEvent mr(QMouseEvent::MouseButtonRelease, element.geometry().center(), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
    QCoreApplication::sendEvent(view(), &mr);
    return true;
}

void QWebPageBot::sendMoveEvent(const QPoint point)
{
    QMouseEvent mp(QMouseEvent::MouseMove, point, Qt::NoButton, Qt::NoButton, Qt::NoModifier);
    QCoreApplication::sendEvent(view(), &mp);
}

QPoint QWebPageBot::getCoordsBySelector(const QString & selector )
{
    QWebElement element = this->mainFrame()->findFirstElement(selector);

    return element.geometry().center();
}
