#ifndef QNETWORKCOOKIEJARPROFILE_H
#define QNETWORKCOOKIEJARPROFILE_H

#include <QNetworkCookieJar>

class QNetworkCookieJarProfile : public QNetworkCookieJar
{
    Q_OBJECT
public:
    explicit QNetworkCookieJarProfile(QObject *parent = 0);
    QList<QNetworkCookie> getAllCookies();

signals:

public slots:

};

#endif // QNETWORKCOOKIEJARPROFILE_H
