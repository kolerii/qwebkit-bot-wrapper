#ifndef QWEBVIEWBOT_H
#define QWEBVIEWBOT_H

#include <QWebView>
#include "qwebpagebot.h"
#include <QtWebKit/QWebElementCollection>
#include <QMutex>
#include <mainwindow.h>

class MainWindow;

class QWebViewBot : public QWebView
{
    Q_OBJECT
public:
    explicit QWebViewBot(QWidget *parent = 0);
    QWebPageBot* getPage();

    bool status_load;

    MainWindow * parent;
    QString useragent;

    QPoint lastPoint;


    QWebView* createWindow(QWebPage::WebWindowType);
signals:
    void cursorMoved( QPoint );

private slots:
    void set_status_loading();
    void set_status_loaded(bool);

public slots:
    void paintEvent( QPaintEvent *);
    void wheelEvent( QWheelEvent *);
    void resizeEvent( QResizeEvent *);
    void send_wheel_scroll(const int &, const Qt::Orientation&);
    void load_url( const QString& , const QString&);
    void scrollOn( const int&, const Qt::Orientation & );
    void click_on_coords( const int &, const int &);
    bool click_on_first_find( const QString &);
    QPoint get_coords_by_selector( const QString &);
    void setUserAgent( const QString & );

    void setResolution( const int&, const int& );
    void scrollTo( const int&, const Qt::Orientation & );

    int get_element_scrollbar_value( QWebElement &, const Qt::Orientation );

    QWebElementCollection findAllBySelector(const QString& );
    QString getCurrentUrl();
    QUrl getParsedUrl();

    QWebElementCollection get_internal_url(const QString&, const QString&);
    QWebElementCollection get_external_url(const QString& );
    void send_mouse_move_event(const QPoint &);
    void send_click_event_on_coords(const QPoint &);
    void send_release_event_on_coords(const QPoint &);

    void send_release_key(const QChar &);
    void send_press_key(const QChar &);
    void send_release_key(const int &);
    void send_press_key(const int &);

    void saveFrameImage(const QString &);
    QPoint getImagePoint(const QString &, const int &);

    QWebElementCollection get_collection_by_selector( const QString & );
    QString get_element_attr( const QWebElement &, const QString & );
    QRect get_element_geometry( const QWebElement &);

private:
    bool shiftPressed;
    bool leftButtonPressed;
};

#endif // QWEBVIEWBOT_H
