#ifndef QWEBPAGEBOT_H
#define QWEBPAGEBOT_H

#include <QWebPage>
#include <QWebView>
#include <QStack>

class QWebPageBot : public QWebPage
{
    Q_OBJECT
public:
    explicit QWebPageBot(QObject *parent = 0);
    QString userAgentForUrl ( const QUrl & url ) const;
    void setAgent( const QString& );
    void load( const QString& );

    QPoint getCoordsBySelector(const QString& );

    void clickOnCoords(int, int);
    bool clickOnFirstFind( const QString& );

    void sendMoveEvent(const QPoint point);

signals:

public slots:
    QString chooseFile(QWebFrame *, const QString &);

private:
    QString _useragent;
    QWebSettings *defaultSettings;

};

#endif // QWEBPAGEBOT_H
