#include "qnetworkcookiejarprofile.h"
#include <QNetworkCookie>

QNetworkCookieJarProfile::QNetworkCookieJarProfile(QObject *parent) :
    QNetworkCookieJar(parent)
{
}

QList<QNetworkCookie> QNetworkCookieJarProfile::getAllCookies()
{
    return this->allCookies();
}
